const express = require('express');
const fs = require('fs');
const { createProxyMiddleware } = require('http-proxy-middleware');
const bodyParser = require('body-parser');


CREDS_FILE = './creds/creds.json';

const app = express();

const getCreds = () => {
    const rawData = fs.readFileSync(CREDS_FILE);
    return JSON.parse(rawData);
};

const fanduelApiProxy = createProxyMiddleware({
    target: 'https://api.fanduel.com',
    changeOrigin: true,
    pathFilter: '/api',
    pathRewrite: (path, req) => path.replace('/api', '/'),
    onProxyReq: (proxyReq, req, res) => {
        proxyReq.removeHeader('postman-token');
        proxyReq.setHeader('Authorization', 'Basic ZWFmNzdmMTI3ZWEwMDNkNGUyNzVhM2VkMDdkNmY1Mjc6');
        proxyReq.setHeader('Origin', 'https://www.fanduel.com');
        proxyReq.setHeader('Referer', 'https://www.fanduel.com/');
        proxyReq.setHeader('Sec-Ch-Ua', `".Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"`);
        proxyReq.setHeader('Sec-Ch-Ua-Mobile', '?0');
        proxyReq.setHeader('Sec-Ch-Ua-Platform', `"Windows"`);
        proxyReq.setHeader('Sec-Fetch-Dest', 'empty');
        proxyReq.setHeader('Sec-Fetch-Mode', 'cors');
        proxyReq.setHeader('Sec-Fetch-Site', 'same-site');
        proxyReq.setHeader('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36');
        proxyReq.setHeader('X-Brand', 'FANDUEL');
        proxyReq.setHeader('X-Currency', 'USD');
        proxyReq.setHeader('X-Geo-Region', '');
    }
});

const fanduelGraphqlProxy = createProxyMiddleware({
    target: 'https://graphql.fanduel.com',
    changeOrigin: true,
    pathFilter: '/graphql',
    pathRewrite: (path, req) => path.replace('/graphql', '/graphql'),
    onProxyReq: (proxyReq, req, res) => {
        proxyReq.removeHeader('postman-token');
        proxyReq.setHeader('Authorization', 'Basic ZWFmNzdmMTI3ZWEwMDNkNGUyNzVhM2VkMDdkNmY1Mjc6');
        proxyReq.setHeader('Origin', 'https://www.fanduel.com');
        proxyReq.setHeader('Referer', 'https://www.fanduel.com/');
        proxyReq.setHeader('Sec-Ch-Ua', `".Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"`);
        proxyReq.setHeader('Sec-Ch-Ua-Mobile', '?0');
        proxyReq.setHeader('Sec-Ch-Ua-Platform', `"Windows"`);
        proxyReq.setHeader('Sec-Fetch-Dest', 'empty');
        proxyReq.setHeader('Sec-Fetch-Mode', 'cors');
        proxyReq.setHeader('Sec-Fetch-Site', 'same-site');
        proxyReq.setHeader('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36');
        proxyReq.setHeader('X-Brand', 'FANDUEL');
        proxyReq.setHeader('X-Currency', 'USD');
        proxyReq.setHeader('X-Geo-Region', '');
    }
});

app.use((req, res, next) => {
    if (req.path.startsWith("/api") || req.path.startsWith("/graphql")) {
        const creds = getCreds();
        req.headers["X-Auth-Token"] = creds.authToken;
        req.headers["X-Geo-Packet"] = creds.geoToken;
        req.headers["X-Px-Context"] = creds.pxContext;
        req.headers["Cookie"] = creds.cookie;
        next();
    } else {
        next()
    }
});

const jsonParser = bodyParser.json();

app.post('/creds', jsonParser, (req, res) => {
    const creds = req.body;
    fs.writeFileSync(CREDS_FILE, JSON.stringify(creds));
    res.status(201).send('Done');
});

app.use('/api', fanduelApiProxy);
app.use('/graphql', fanduelGraphqlProxy);
app.listen(3333, () => {
    console.log('Fanduel API Proxy Listening on 3333!');
});